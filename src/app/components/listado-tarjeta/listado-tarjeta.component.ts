import { Component, OnInit } from '@angular/core';
import { TarjetaCredito } from 'src/app/models/TarjetaCredito';
import { MatTableDataSource } from '@angular/material/table';
@Component({
  selector: 'app-listado-tarjeta',
  templateUrl: './listado-tarjeta.component.html',
  styleUrls: ['./listado-tarjeta.component.css']
})

export class ListadoTarjetaComponent implements OnInit {
displayedColumns:any[]=['titular','numeroTarjeta','fechaExpiracion','clave']


  constructor() { }

  ngOnInit(): void {
  }

}
